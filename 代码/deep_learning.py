# -*- coding: utf-8 -*-
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import tensorflow as tf
from tensorflow.keras import layers 
from tensorflow.keras.layers import Dense, LSTM
import pickle
import matplotlib.pyplot as plt
from machine_learning import plot_estimator, predict_estimator
from sklearn.metrics import mean_absolute_percentage_error

font = {'family' : 'Times New Roman',
'weight' : 'normal',
'size'   : 20,
}
plt.rcParams['font.sans-serif']=['SimHei']
plt.rcParams['axes.unicode_minus'] = False


        
def predict_estimator_ann(column,y_train, y_train_predict, y_test, y_test_predict):
    mape_train = mean_absolute_percentage_error(y_train, y_train_predict)
    mape_test = mean_absolute_percentage_error(y_test, y_test_predict)
    print(f'负荷数据：{column} 使用模型 多层感知器 在训练集和测试集中的 MAPE 分别为: {mape_train},{mape_test}')

def solve_by_ann_together(data, epoches):
    columns = ['电力', '可再生能源', '制冷',\
                            '制热', '气体管道']  
    X = data.iloc[:, :-5]
    y = data.iloc[:, -5:]
    
    split = int(np.round(0.7*X.shape[0]))
    X_train = X.iloc[:split, :]
    X_test = X.iloc[split:, :]
    y_train = y.iloc[:split]
    y_test = y.iloc[split:]
    history, ann = build_ann(X_train, X_test, y_train, y_test, epoches=epoches)
    y_train_pred = ann.predict(X_train)
    y_test_pred = ann.predict(X_test)
    
    plot_history(history, '多层感知器')
    for i in range(5):
        col = columns[i]
        plot_estimator(y_train.iloc[:,i], y_train_pred[:,i]\
                        , y_test.iloc[:,i], y_test_pred[:,i], col, '多层感知器')
                        
        predict_estimator_ann(col, y_train.iloc[:,i], y_train_pred[:,i],\
                            y_test.iloc[:,i], y_test_pred[:,i])
        
def solve_by_lstm_together(data, epoches):
    columns = ['电力', '可再生能源', '制冷',\
                            '制热', '气体管道']  
    X = data.iloc[:, :-5]
    y = data.iloc[:, -5:]
    
    split = int(np.round(0.7*X.shape[0]))
    X_train = X.iloc[:split, :]
    X_test = X.iloc[split:, :]
    
    X_train = np.array(X_train).reshape((X_train.shape[0], 1, X_train.shape[1]))
    X_test = np.array(X_test).reshape((X_test.shape[0], 1, X_test.shape[1]))
    
    y_train = y.iloc[:split, :]
    y_test = y.iloc[split:, :]  
    y_train = np.array(y_train).reshape((y_train.shape[0], 1, y_train.shape[1]))
    y_test = np.array(y_test).reshape((y_test.shape[0], 1, y_test.shape[1]))
    X = np.array(X).reshape((X.shape[0], 1, X.shape[1]))
    y = np.array(y).reshape((y.shape[0], 1, y.shape[1]))
    history, lstm = build_lstm(X, X_test, y, y_test, epoches=epoches)
    y_train_pred = lstm.predict(X_train)
    y_test_pred = lstm.predict(X_test)
    print(y_train_pred.shape)
    plot_history(history, 'LSTMs')
    for i in range(5):
        col = columns[i]
        plot_estimator(y_train[:,0, i], y_train_pred[:,0, i]\
                        , y_test[:,0, i], y_test_pred[:,0, i], col, 'LSTMs')
                        
        predict_estimator_ann(col, y_train[:,0, i], y_train_pred[:,0, i],\
                            y_test[:,0, i], y_test_pred[:,0, i])
        

def prepare_train_test_data(data):
    X = data.iloc[:, :-5]
    y = data.iloc[:, -5:]
    split = int(np.round(0.7*X.shape[0]))
    X_train = X.iloc[:split, :]
    X_test = X.iloc[split:, :]
    
    #X_train = np.array(X_train).reshape((X_train.shape[0], 1, X_train.shape[1]))
    #X_test = np.array(X_test).reshape((X_test.shape[0], 1, X_test.shape[1]))
    y_train = y.iloc[:split, :]
    y_test = y.iloc[split:, :]
    
    return X_train, X_test, y_train, y_test
    
def build_ann(X_train, X_test, y_train, y_test,
            units_list=[50, 100, 50, 25],
            activation='relu',
            init='glorot_uniform',
            rate=0.1,
            epoches=50,
            fit_flag=True):
                
    # 寻优算法
    
    optimizer = tf.keras.optimizers.Adam(
                                learning_rate=0.01, \
                                beta_1=0.9, beta_2=0.999, \
                                epsilon=1e-07, amsgrad=False,)
    '''
    根据参数，搭建一个多层感知器模型
    '''
    ann = tf.keras.Sequential()
    for units in units_list:
        # 添加隐藏层
        ann.add(layers.Dense(units=units,
                        activation=activation,
                        kernel_initializer=init))
        # 添加 Dropout 层
        ann.add(layers.Dropout(rate=rate))
    # 输出层
    ann.add(layers.Dense(units=5,
                        activation='linear',
                        kernel_initializer=init))

    ann.compile(loss='mse', 
                optimizer=optimizer)

    
    if fit_flag:
        history = ann.fit(X_train,y_train,epochs = epoches,
                        batch_size=100, validation_data=(X_test,y_test))
    
    return history, ann


def build_lstm(X_train, X_test, y_train, y_test, epoches=50, fit_flag=True): 
    
    lstm_model = tf.keras.models.Sequential([
        tf.keras.layers.LSTM(128, return_sequences=True),
        tf.keras.layers.Dense(units=5, activation='linear')
    ])
    
    optimizer = tf.keras.optimizers.Adam(
                                learning_rate=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-07, amsgrad=False,
                                            )

    lstm_model.compile(loss='mse', optimizer=optimizer)
    if fit_flag:
        history = lstm_model.fit(X_train,y_train,epochs = epoches,
                        batch_size=300, validation_data=(X_test,y_test))
    return history, lstm_model
                

        
def plot_history(history, name):
    '''
    画出训练历史
    '''
    fig =  plt.figure(figsize=(8,4))
    plt.subplot(1,2,1)
    data = history.history['loss']
    plt.plot(data)
    plt.xlabel('迭代次数')
    plt.ylabel('损失函数值（训练集）')
    
    plt.subplot(1,2,2)
    data = history.history['val_loss']
    plt.plot(data)
    plt.xlabel('迭代次数')
    plt.ylabel('损失函数值（测试集）')
    fig.savefig(f'../图片/{name}训练过程.png')
    plt.show()


if __name__ == '__main__':
    
    data = pd.read_excel(r'../附件/中间数据/窗口滑动_步长为5.xlsx', index_col=0)
    data.dropna(inplace=True)    

    solve_by_ann_together(data, epoches=500)
    solve_by_lstm_together(data, epoches=500)



