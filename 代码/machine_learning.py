# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import pickle
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.metrics import mean_squared_error, make_scorer
from sklearn.metrics import mean_absolute_percentage_error
import matplotlib.pyplot as plt


font = {'family' : 'SimHei',
'weight' : 'normal',
'size'   : 10,
}
plt.rcParams['font.sans-serif']=['SimHei']    #画图时使用中文字体
plt.rcParams['axes.unicode_minus'] = False
plt.rcParams["figure.figsize"] = (6,4)

def select_knn(X, y, scorer, cv=5, verbose=True):
    """"筛选kNN算法的最合适参数k"""
    grid = {'n_neighbors':[ 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43]}
      
    grid_search = GridSearchCV(KNeighborsRegressor(),\
                                param_grid=grid,
                                cv=cv,
                                scoring=scorer,
                                n_jobs=-1)
    
    grid_search.fit(X, y)
    if verbose:
        print('kNN 最佳参数： ', grid_search.best_params_)
    return grid_search.best_params_

def select_svr(X, y, scorer, cv=5, verbose=True):
    '''
    选择 SVR 最合适参数
    '''
    
    grid = {
            'C':[0.01, 0.05, 0.1, 0.25, 0.5, 0.75, 1, 1.25],
            'kernel':['linear','rbf','poly'],
            'epsilon':[0,0.001, 0.05, 0.01, 0.05, 0.1]
            }

    grid_search = GridSearchCV(SVR(),\
                                param_grid=grid,
                                cv=cv,
                                scoring=scorer,
                                n_jobs=-1)
    
    grid_search.fit(X, y)
    if verbose:
        print('SVR 最佳参数： ', grid_search.best_params_)
    return grid_search.best_params_

def select_dtr(X, y, scorer, cv=5, verbose=True):
    '''
    筛选决策树的最佳参数
    '''
    grid = {'max_depth':[4, 9, 13, 17, 21, 25],\
            'ccp_alpha':[0,0.01,0.05,0.1,0.2,0.3,0.4,0.5]} 
    grid_search = GridSearchCV(DecisionTreeRegressor(),
                                param_grid=grid, cv=cv, \
                                scoring=scorer,
                                n_jobs=-1)
    grid_search.fit(X, y)
    if verbose:
        print('决策树最佳参数： ', grid_search.best_params_) 
    return grid_search.best_params_

def select_rf(X, y, scorer, cv=5, verbose=True):
    '''
    筛选随机森林的最佳参数
    '''
    grid = {'n_estimators':[5, 15, 25, 35, 45, 50, 65, 75, 85, 95]}
    grid_search = GridSearchCV(RandomForestRegressor(max_samples=0.67,\
                                max_features=0.33, max_depth=5), \
                                param_grid=grid, cv=cv,\
                                scoring=scorer,
                                n_jobs=-1)
    grid_search.fit(X, y)
    if verbose:
        print('随机森林最佳参数: ', grid_search.best_params_)
    return grid_search.best_params_


def select_ada(X, y, scorer, cv=5, verbose=True):
    '''
    筛选 AdaBoost 的最佳参数，其中基模型为逻辑回归模型
    '''
    grid = {'n_estimators':[5, 15, 25, 35, 45, 50, 65, 75, 85, 95]}
    grid_search = GridSearchCV(AdaBoostRegressor( \
                                base_estimator=LinearRegression()),\
                                param_grid=grid,
                                cv=cv,
                                scoring=scorer,
                                n_jobs=-1)
    
    
    grid_search.fit(X, y)
    if verbose:    
        print('AdaBoost 最佳参数： ', grid_search.best_params_)
    return grid_search.best_params_

def select_model(X, y, scorer, cv=5, verbose=True):
    '''
    将筛选参数整合为一个函数
    '''
    knn_param = select_knn(X, y, scorer, cv, verbose=verbose)
    svc_param = select_svr(X, y, scorer, cv, verbose=verbose)
    dtc_param = select_dtr(X, y, scorer, cv, verbose=verbose)
    rf_param = select_rf(X, y, scorer, cv, verbose=verbose)
    ada_param = select_ada(X, y, scorer, cv, verbose=verbose)
    return knn_param, svc_param, dtc_param, rf_param, ada_param

def plot_line(y_train, y_test, col):
    '''
    画出训练集、测试集
    '''
    fig, ax = plt.subplots()
    x_train = len(y_train.values)
    x_test = len(y_test.values)
    plt.plot(y_train.values, c='b', label=' 训练集')
    plt.plot(range(x_train,x_train+x_test), y_test.values, 'r--', label='测试集')
    plt.xlabel('日期序列')
    plt.ylabel(f'{col}')
    plt.title(f'{col} 训练集和测试集')
    plt.grid()
    fig.savefig(f'../图片/{col}.png')
    plt.legend(prop=font)    
    plt.show()
    
def plot_estimator(y_train, y_train_pred, y_test, y_test_pred, col, estimator):
    '''
    画出训练集、测试集
    '''
    fig = plt.figure(figsize=(10, 4))
    plt.subplot(1,2,1)
    if isinstance(y_train, pd.Series):
        plt.plot(y_train.values, c='b', label='实际数据')
    else:
        plt.plot(y_train, c='b', label='实际数据')
        
    plt.plot(y_train_pred, c='r', linestyle='--', label='拟合数据')
    plt.xlabel('日期序列')
    plt.ylabel(col)
    plt.legend(prop=font)
    plt.title(f'{estimator}_训练集')
    
    plt.subplot(1,2,2)
    if isinstance(y_test, pd.Series):
        plt.plot(y_test.values, c='b', label='实际数据')
    else:
        plt.plot(y_test, c='b', label='实际数据')
    plt.plot(y_test_pred, c='r', linestyle='--', label='拟合数据')
    plt.xlabel('日期序列')
    plt.ylabel(col)
    plt.legend(prop=font)
    plt.title(f'{estimator}_测试集')
    
    fig.savefig(f'../图片/{estimator}_{col}.png')
    plt.show()

def predict_estimator(estimator, estimator_name, column, X_train, X_test, y_train, y_test):
    estimator.fit(X_train, y_train)
    y_train_predict = estimator.predict(X_train)
    mape_train = mean_absolute_percentage_error(y_train, y_train_predict)
    
    y_test_predict = estimator.predict(X_test)
    mape_test = mean_absolute_percentage_error(y_test, y_test_predict)
    
    plot_estimator(y_train, y_train_predict, y_test, y_test_predict, column, estimator_name)
    
    print(f'模型 {estimator} 的在训练集和测试集中的 MAPE 分别为: {mape_train},{mape_test}')

def return_score(X, y, column, verbose=True, \
            knn_param={'n_neighbors':43}, \
            svr_param={'C': 0.01, 'kernel': 'poly', 'epsilon':0.1},\
            dtr_param={'ccp_alpha':0.1, 'max_depth':4}, \
            rf_param={'n_estimators':15},\
            ada_param={'n_estimators':95},
            ):
                
    """根据上述最优参数，构建模型"""
    lr = LinearRegression()
    knn = KNeighborsRegressor(n_neighbors=knn_param['n_neighbors'])
    svr = SVR(C=svr_param['C'], kernel=svr_param['kernel'],
              epsilon=svr_param['epsilon'])
              
    dtr = DecisionTreeRegressor(max_depth=dtr_param['max_depth'],
                                ccp_alpha=dtr_param['ccp_alpha'])
                                
    rf = RandomForestRegressor(n_estimators=rf_param['n_estimators'],\
                                max_samples=0.67,\
                                max_features=0.33, max_depth=5)
    ada = AdaBoostRegressor(base_estimator=lr,\
                            n_estimators=ada_param['n_estimators'])
    
    # 拆分数据集
    split = int(np.round(0.7*X.shape[0]))
    X_train = X.iloc[:split, :]
    X_test = X.iloc[split:, :]
    y_train = y.iloc[:split]
    y_test = y.iloc[split:]
    
    if verbose:
        plot_line(y_train, y_test, column)
        estimators = ['lr', 'svr', 'knn', 'dtr', 'rf', 'ada']
        for estimator in estimators:
            predict_estimator(locals()[estimator], estimator, column, X_train, X_test, y_train, y_test)
    
    
    
def select_model_all(data, scorer, cv=1):
    columns = ['电力', '可再生能源', '制冷',\
                            '制热', '气体管道']  
    X = data.iloc[:, :-5]
    for i in range(5):
        i = -(i+1)
        col = columns[i]
        print('当前预测列为： ', col)
        # 解耦
        y = data.iloc[:, i]

        knn_param, svr_param, \
                    dtr_param, rf_param, ada_param = select_model(X, y, scorer, cv=cv, verbose=True)
        
        return_score(X, y, col, knn_param=knn_param, \
                    svr_param=svr_param, dtr_param=dtr_param, \
                    rf_param=rf_param, ada_param=ada_param)
        
        
    
if __name__ == '__main__':
    data = pd.read_excel(r'../附件/中间数据/窗口滑动_步长为5.xlsx', index_col=0)
    data.dropna(inplace=True)
    scorer = make_scorer(mean_absolute_percentage_error)
    # y 为当前负荷
    select_model_all(data, scorer, cv=5)
