# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import pickle
from datetime import datetime as dt
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from series_to_supervised import series_to_supervised

plt.rcParams['font.sans-serif']=['SimHei']    #画图时使用中文字体
plt.rcParams['axes.unicode_minus'] = False
plt.rcParams["figure.figsize"] = (6,12)

def data_generate():
    # 遍历文件夹的数据
    data_set = ['2016.csv', '2017.csv', '2018.csv', '2019.csv', '2020.csv']
    final_data = pd.DataFrame(columns = ['Year', 'Month', 'Day', '电力/KW', '可再生能源/WK',\
                    '制冷/(Tons/h)', '制热/(mmBTU/h)', '气体管道/(Tons/h)'])
    # 从 CSV 文件中读取数据
    for file_path in data_set:
        data = pd.read_csv(f'../附件/ASU/{file_path}')
        data = data.loc[:, ['Year', 'Month', 'Day', 'KW', 'KWS', 'CHWTON',\
                                'HTmmBTU', 'Combined Tons Carbon']]
        # 更改列名，使得更好看些
        columns = ['Year', 'Month', 'Day', '电力/KW', '可再生能源/WK',\
                    '制冷/(Tons/h)', '制热/(mmBTU/h)', '气体管道/(Tons/h)']   
        data.columns = columns
        
        # 求解四分位数
        q = data.quantile([.25, .75]).T
        iqrs = 1.5*(q[0.75] - q[0.25])
        
        # 异常值订正
        for col in columns:
            iqr = iqrs[col]
            data[col].loc[data[col] >= q.loc[col][0.75]+iqr] = q.loc[col][0.75]
            data[col].loc[data[col] <= q.loc[col][0.25]-iqr] = q.loc[col][0.25]
        
        # 将一个个文件拼接成一个总的 Dataframe
        final_data = pd.concat([final_data, data], axis=0)
    
    # 增加日期序列
    final_data['Date'] = final_data['Year'].astype('int').astype('str') + '-' + \
            final_data['Month'].astype('int').astype('str') + '-' +  \
            final_data['Day'].astype('int').astype('str')
    # 将字符串转换为 日期 类型   
    final_data['Date'] = final_data['Date'].apply(lambda x: dt.strptime(x, '%Y-%m-%d'))
    # 将其他列删除掉
    final_data.drop(['Year', 'Month', 'Day'], axis=1, inplace=True)
    return final_data.set_index('Date')

def plot_line(data, verbose=False):
    '''
    画线型图
    '''
    cols = data.columns
    for col in cols:
        fig = plt.figure(figsize=(6,4))
        plt.plot(data[col])
        plt.ylabel(col)
        plt.grid()
        plt.title(f'{col}负荷数据')
        file_path = str(col).replace('/', '')
        fig.savefig(f'../图片/{file_path}线图.png')
        if verbose:
            plt.show()
    
def plot_hist(data, verbose=False):
    '''
    画频数图
    '''
    cols = data.columns
    for col in cols:
        fig = plt.figure(figsize=(6,4))
        plt.hist(data[col], 50, density=True)
        plt.ylabel('频率密度图')
        plt.xlabel(f'{col}负荷数据')
        plt.grid()
        file_path = str(col).replace('/', '')
        fig.savefig(f'../图片/{file_path}hist图.png')
        if verbose:
            plt.show()

    
def plot_auto(data, verbose=False):
    '''
    画自相关图
    '''
    cols = data.columns
    for col in cols:
        fig = plt.figure(figsize=(6,4))
        ax = plt.gca()
        pd.plotting.autocorrelation_plot(data[col], ax=ax)   
        plt.ylabel('自相关系数')
        plt.xlabel('时间延迟')
        plt.title(f'{col}负荷数据')
        ax.grid()
        plt.grid()
        file_path = str(col).replace('/', '')
        fig.savefig(f'../图片/{file_path}auto图.png')
        if verbose:
            plt.show()


def data_test(data):
    '''
    
    '''
    for col in data.columns:
        result = adfuller(data[col])
        print('%s ADF Statistic: %f' %(col, result[0]))
        print('p-value: %f' % result[1])
        print('Critical Values:')
        for key, value in result[4].items():
            print('\t%s: %.3f' % (key, value))
    


if __name__ == '__main__':
    # 准备数据集
    data = data_generate()
    plot_line(data, verbose=True)
    plot_hist(data, verbose=True)
    plot_auto(data, verbose=True)
    # 将数据保存为 excel 表格
    data.to_excel(r'../附件/中间数据/原始数据.xlsx')
    # 数据统计描述与保存
    data_describe = data.describe()
    data_describe.to_excel(r'../附件/中间数据/统计描述.xlsx')
    # 数据标准化，将其转换为均值为 0，方差为 1 的数据
    scaler = StandardScaler()
    cols = data.columns
    data_after = scaler.fit_transform(data)
    data_after = pd.DataFrame(data=data_after, columns=cols)
    data_after['Date'] = data.index
    data_after = data_after.set_index('Date')
    # 将标准化数据保存
    data_after.to_excel(r'../附件/中间数据/标准化后.xlsx')
    # 将标准化模型保存（数据的原始均值、标准差）保存为 .pkl 文件
    pickle.dump(scaler, open(r'../附件/中间数据/scaler.pkl', 'wb'))
    
    # 使用窗口 size 为 6 的滑动窗口法，将数据保存起来。
    data_window = series_to_supervised(data_after, n_in=5, n_out=1, dropnan=False)
    data_window.to_excel(r'../附件/中间数据/窗口滑动_步长为5.xlsx')
    
    # 删除缺失数据， delete by list
    data_window.dropna(inplace=True)
    
